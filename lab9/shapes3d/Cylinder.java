package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getRadius() {
        return super.getRadius();
    }

    public void setRadius(double radius) {
        super.setRadius(radius);
    }

    public double Area() {
        return 2 * super.Area() + (super.Perimeter() * this.height);
    }

    public double Volume() {
        return super.Area() * this.height;
    }

    @Override
    public String toString() {
        return "Cylinder's height: "+ this.height+ 
        " radius: "+ super.getRadius()+ " Area: "+ 
        this.Area()+ " Volume: "+ this.Volume();
    }

    
}