package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(double side) {
        super(side);
    }

    public double getSide() {
        return super.getSide();
    }

    public void setSide(double side) {
        super.setSide(side);
    }

    public double Area() {
        return 6 * super.Area();
    }

    public double Volume() {
        return super.Area() * super.getSide();
    }

    @Override
    public String toString() {
        return "Cube's side: "+ super.getSide()+ " Area: "+
        this.Area()+ " Volume: "+ this.Volume();
    }

    
}