import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cube;
import shapes3d.Cylinder;

public class test {
    public static void main(String[] args){
        Circle ci = new Circle(5);
        Square sq = new Square(10);
        Cylinder cy = new Cylinder(15, 20);
        Cube cu = new Cube(25);

        System.out.println(ci.toString());
        System.out.println(sq.toString());
        System.out.println(cy.toString());
        System.out.println(cu.toString());
    }
}