public class Main{
    public static void main(String[] args){
        Point p = new Point(8,10);
        Rectangle r = new Rectangle(p,5,9);
        Circle c1 = new Circle(10,p);
        System.out.println("Rectangle's area: "+ r.area());
        System.out.println("Rectangle's perimeter: "+ r.perimeter());
        Point[] points = r.corners();
        for(int i = 0; i < 4; i++){
            System.out.println("Rectangle's corner "+ (i +1)+" "+ points[i].xCoord + "," + points[i].yCoord);
        }
        System.out.println("");
        System.out.println("Circle's area: "+ c1.area());
        System.out.println("Circle's perimeter: "+ c1.perimeter());
        Point p2 = new Point(8,15);
        Circle c2 = new Circle(3,p2);
        System.out.println("Circle1 and Circle2 intersect: "+ c1.intersect(c2));

    }
}