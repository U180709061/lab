public class GCDLoop{
    public static int gcd(int a, int b){
        int r0;
        while(b != 0){
            r0 = a % b;
            a = b;
            b = r0;
        }
        return a;   
    }    
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
       
        System.out.println(gcd(a,b));
    }
}
