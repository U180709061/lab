package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for(Object o : shapes){
			if(o instanceof Circle){
				totalArea += ((Circle)o).area();
			}
			else if(o instanceof Rectangle){
				totalArea += ((Rectangle)o).area();
			}
			else if(o instanceof Square){
				totalArea += ((Square)o).area();
			}
		}
		return totalArea;
	}
	
	public void addShape(Object o){
		shapes.add(o);
	}
}
