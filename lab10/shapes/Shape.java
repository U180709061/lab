package shapes;

public abstract class Shape {

    abstract public double area();
}