package shapes;

public class Square extends Shape {
    private double side;

    

    public double area(){
        return this.side * this.side;
    }

    public Square(double side) {
        this.side = side;
    }
}