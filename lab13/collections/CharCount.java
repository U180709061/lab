package collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class CharCount {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a string: ");
        String input = sc.nextLine();
        System.out.println(occurence(input));
        SortedMap sm = ordered_occurence(input);
        Set s = sm.entrySet();
        Iterator i = s.iterator();
        while (i.hasNext()) {
            Map.Entry m = (Map.Entry) i.next();
            char key = (Character) m.getKey();
            int value = (Integer) m.getValue();
            System.out.println(key+ " "+ value);
        }
        
        sc.close();
    }

    public static HashMap occurence(String input) {
        Map map = new HashMap();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (map.containsKey(c)) {
                int count = (Integer)map.get(c);
                map.put(c, count + 1);
            } else {
                map.put(c, 1);
            }
        }
        return (HashMap) map;
    }
    
    public static SortedMap ordered_occurence(String input) {
        SortedMap<Character, Integer> sm = new TreeMap<Character, Integer>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (sm.containsKey(c)) {
                int count = sm.get(c);
                sm.put(c, count + 1);
            } else {
                sm.put(c, 1);
            }
        }
        return sm;
    }
}