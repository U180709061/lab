public class Test{
    public static void main(String[] args) {
        
        Circle c1 = new Circle();
        Circle c2 = new Circle(5.0);
        Circle c3 = new Circle(7.0, "yellow");

        System.out.println(c1.toString());
        System.out.println(c2.toString());
        System.out.println(c3.toString());
        System.out.println();

        Cylinder cy1 = new Cylinder();
        Cylinder cy2 = new Cylinder(4.0);
        Cylinder cy3 = new Cylinder(20.0, 5.0);
        Cylinder cy4 = new Cylinder(10.0, 6.0, "black");

        System.out.println(cy1.toString());
        System.out.println(cy2.toString());
        System.out.println(cy3.toString());
        System.out.println(cy4.toString());

    }
}